// GTK_GEOMOPT_DIALOG.H : write a short description here...

// Copyright (C) 2002 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef GTK_GEOMOPT_DIALOG_H
#define GTK_GEOMOPT_DIALOG_H

//#include "ghemicalconfig2.h"

#include "gtk_glade_dialog.h"

#include <ghemical/geomopt.h>

/*################################################################################################*/

class gtk_geomopt_dialog : public gtk_glade_dialog
{
	protected:
	
	geomopt_param * param;
	
	GtkWidget * dialog;
	
	GtkWidget * entry_nsteps;
	GtkWidget * entry_grad;
	GtkWidget * entry_delta_e;
	
	public:
	
	gtk_geomopt_dialog(geomopt_param *);
	~gtk_geomopt_dialog(void);
	
	static void handler_Destroy(GtkWidget *, gpointer);
	
	static void handler_ButtonOK(GtkWidget *, gpointer);
	static void handler_ButtonCancel(GtkWidget *, gpointer);
};

/*################################################################################################*/

#endif	// GTK_GEOMOPT_DIALOG_H

// eof
