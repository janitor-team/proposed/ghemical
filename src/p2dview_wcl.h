// P2DVIEW_WCL.H : write a short description here...

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ghemicalconfig2.h"

#ifndef P2DVIEW_WCL_H
#define P2DVIEW_WCL_H

#include "ac_stor_wcl.h"

#include <ghemical/engine.h>

/*################################################################################################*/

struct p2d_data
{
	double c1;		// coordinate 1 (say, x-coordinate)
	double c2;		// coordinate 2 (say, y-coordinate)
	double v;		// value
	
	int ac_i;		// atom coordinate data
};

class p2dview_wcl :
	public ac_stor_wcl
{
	protected:
	
	char * name1;
	char * name2;
	char * namev;
	
	vector<p2d_data> dv;
	
	f64 min1; f64 max1;
	f64 min2; f64 max2;
	f64 minv; f64 maxv;
	
	public:
	
	p2dview_wcl(const char *, const char *, const char *);
	virtual ~p2dview_wcl(void);
	
	void AddData(double, double, double);
	void AddDataWithAC(double, double, double, engine *);
	virtual void Finalize(void);
	
	void ButtonEvent(int, int);		// virtual
	void MotionEvent(int, int);		// virtual
	
	void UpdateWnd(void);			// virtual
	
	void InitGL(void);			// virtual
	void RenderGL(rmode);			// virtual
	
	protected:
	
	void SetColor(GLfloat);
};

/*################################################################################################*/

#endif	// P2DVIEW_WCL_H

// eof
