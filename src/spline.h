// SPLINE.H : a "B-spline" class for ribbon models.

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef SPLINE_H
#define SPLINE_H

//#include "ghemicalconfig2.h"

class spline;

/*################################################################################################*/

#include <ghemical/typedef.h>

/*################################################################################################*/

/// This is a "##B-spline" class for those ribbon models...

class spline
{
	private:
	
	i32s order;
	i32s ncp; i32s nk;
	
	fGL * knot;
	fGL * weight;
	fGL ** cpcrd;		// no data, just the pointers...
	
	public:
	
	spline(i32s p1, i32s p2);
	~spline(void);
	
	void Compute(fGL, fGL *);
	
	void SetPoint(i32s p1, fGL * p2) { cpcrd[p1] = p2; }
	void SetKnot(i32s p1, fGL p2) { knot[p1] = p2; }
	
	i32s GetOrder(void) { return order; }
};

/*################################################################################################*/

#endif	// SPLINE_H

// eof
