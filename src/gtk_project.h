// GTK_PROJECT.H : classes for GTK2 user interface.

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef GTK_PROJECT_H
#define GTK_PROJECT_H

//#include "ghemicalconfig2.h"

class gtk_project;

/*################################################################################################*/

#include <ghemical/model.h>

#include "project.h"

#include "gtk_simple_dialogs.h"
#include "gtk_progress_dialog.h"

#include <gtk/gtk.h>

#include <vector>
using namespace std;

/*################################################################################################*/

/**	This will contain gtk-dependent aspects of the "##project" class.
*/

class gtk_project : public project
{
	protected:
	
	friend class gtk_file_open_dialog;
	friend class gtk_file_save_dialog;
	
	public:
	
	gtk_project(void);
	void DoSafeStart(void);
	
	virtual ~gtk_project(void);
	
	static oglview_wcl * GetClient(GtkWidget *);
	
#ifdef ENABLE_THREADS
	
	void ThreadLock(void);			// virtual
	void ThreadUnlock(void);		// virtual
	
#else	// ENABLE_THREADS
	
	void NoThreadsIterate(void);		// virtual
	
#endif	// ENABLE_THREADS
	
	bool SetProgress(double, double*);	// virtual
	
	protected:
	
	gtk_progress_dialog * pd;
	
	public:
	
	void CreateProgressDialog(const char *, bool, int, int);	// virtual
	void DestroyProgressDialog(void);				// virtual
	
// the GUI threading model is explained here:
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// the "main thread" of the program handles gtk_main() loop and callbacks.
// in ProcessCommandString() a single "sub-thread" can be started for certain
// operations, if the program is compiled with ENABLE_THREADS set. therefore all
// operations in the process_job_XXX() methods below must be made thread-safe
// in the "gdk_threads" sense. THIS IS STILL EXPERIMENTAL STUFF!!!

// the GUI non-threaded operatios is like this:
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// gtk_main_iteration() is called from libghemical level in order to keep the
// GUI responding ; so this is something like "windows-95"-multitasking stuff...
// but that's OK since this is a fool-proof solution and responsive enough...
	
	void start_job_GeomOpt(jobinfo_GeomOpt *);		// virtual
	static gpointer process_job_GeomOpt(gpointer);
	
	void start_job_MolDyn(jobinfo_MolDyn *);		// virtual
	static gpointer process_job_MolDyn(gpointer);
	
	void start_job_RandomSearch(jobinfo_RandomSearch *);	// virtual
	static gpointer process_job_RandomSearch(gpointer);
	
	// gtk-implementations of creation/removal of views.
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	base_wnd * CreateGraphicsWnd(bool);	// virtual
	void DestroyGraphicsWnd(base_wnd *);	// virtual
	
	base_wnd * CreatePlot1DWnd(bool);			// virtual
	base_wnd * CreatePlot2DWnd(bool);			// virtual
	base_wnd * CreateEnergyLevelDiagramWnd(bool);		// virtual
	base_wnd * CreateReactionCoordinatePlotWnd(bool);	// virtual
	base_wnd * CreateGenericProteinChainWnd(bool);		// virtual
	void DestroyPlottingWnd(base_wnd *);			// virtual
	
	void Message(const char *);		// virtual
	void WarningMessage(const char *);	// virtual
	void ErrorMessage(const char *);	// virtual
	bool Question(const char *);		// virtual
	void PrintToLog(const char *);		// virtual
	
// the popup-menu callbacks start here ; the popup-menu callbacks start here
// the popup-menu callbacks start here ; the popup-menu callbacks start here
// the popup-menu callbacks start here ; the popup-menu callbacks start here
	
	static void popup_FileImport(GtkWidget *, gpointer);
	static void popup_FileExport(GtkWidget *, gpointer);
	static void popup_FileExportGraphics(GtkWidget *, gpointer);
	static void popup_FileExtra1(GtkWidget *, gpointer);
	static void popup_FileExtra2(GtkWidget *, gpointer);	// this is just a stub for a new "extra"-item...
	
	static void popup_SelectAll(GtkWidget *, gpointer);
	static void popup_SelectNone(GtkWidget *, gpointer);
	static void popup_InvertSelection(GtkWidget *, gpointer);
	
	static void popup_HideSelected(GtkWidget *, gpointer);
	static void popup_ShowSelected(GtkWidget *, gpointer);
	static void popup_LockSelected(GtkWidget *, gpointer);
	static void popup_UnlockSelected(GtkWidget *, gpointer);
	static void popup_DeleteSelected(GtkWidget *, gpointer);
	
	static void popup_SelectModeAtom(GtkWidget *, gpointer data);
	static void popup_SelectModeResidue(GtkWidget *, gpointer data);
	static void popup_SelectModeChain(GtkWidget *, gpointer data);
	static void popup_SelectModeMolecule(GtkWidget *, gpointer data);
	
	static void popup_ViewsNewELD(GtkWidget *, gpointer);
	static void popup_ViewsNewSSC(GtkWidget *, gpointer);
	
	static void popup_RModeBallStick(GtkWidget *, gpointer);
	static void popup_RModeVanDerWaals(GtkWidget *, gpointer);
	static void popup_RModeCylinders(GtkWidget *, gpointer);
	static void popup_RModeWireframe(GtkWidget *, gpointer);
	static void popup_RModeNothing(GtkWidget *, gpointer);
	
	static void popup_CModeElement(GtkWidget *, gpointer);
	static void popup_CModeSecStruct(GtkWidget *, gpointer);
	static void popup_CModeHydPhob(GtkWidget *, gpointer);
	
	static void popup_LModeIndex(GtkWidget *, gpointer);
	static void popup_LModeElement(GtkWidget *, gpointer);
	static void popup_LModeFCharge(GtkWidget *, gpointer);
	static void popup_LModePCharge(GtkWidget *, gpointer);
	static void popup_LModeAtomType(GtkWidget *, gpointer);
	static void popup_LModeBuilderID(GtkWidget *, gpointer);
	static void popup_LModeBondType(GtkWidget *, gpointer);
	static void popup_LModeResidue(GtkWidget *, gpointer);
	static void popup_LModeSecStruct(GtkWidget *, gpointer);
	static void popup_LModeNothing(GtkWidget *, gpointer);
	
	static void popup_ObjRibbon(GtkWidget *, gpointer);
	static void popup_ObjEPlane(GtkWidget *, gpointer);
	static void popup_ObjEVolume(GtkWidget *, gpointer);
	static void popup_ObjESurface(GtkWidget *, gpointer);
	static void popup_ObjEVDWSurface(GtkWidget *, gpointer);
	static void popup_ObjEDPlane(GtkWidget *, gpointer);
	static void popup_ObjEDSurface(GtkWidget *, gpointer);
	static void popup_ObjMOPlane(GtkWidget *, gpointer);
	static void popup_ObjMOVolume(GtkWidget *, gpointer);
	static void popup_ObjMOSurface(GtkWidget *, gpointer);
	static void popup_ObjMODPlane(GtkWidget *, gpointer);
	static void popup_ObjMODVolume(GtkWidget *, gpointer);
	static void popup_ObjMODSurface(GtkWidget *, gpointer);
	
	static void popup_ObjectsDeleteCurrent(GtkWidget *, gpointer);
	
	static void popup_CompSetup(GtkWidget *, gpointer);
	static void popup_CompEnergy(GtkWidget *, gpointer);
	static void popup_CompGeomOpt(GtkWidget *, gpointer);
	static void popup_CompMolDyn(GtkWidget *, gpointer);
	static void popup_CompRandomSearch(GtkWidget *, gpointer);
	static void popup_CompSystematicSearch(GtkWidget *, gpointer);
	static void popup_CompMonteCarloSearch(GtkWidget *, gpointer);
	static void popup_CompTorsionEnergyPlot1D(GtkWidget *, gpointer);
	static void popup_CompTorsionEnergyPlot2D(GtkWidget *, gpointer);
	static void popup_CompPopAnaElectrostatic(GtkWidget *, gpointer);
	static void popup_CompTransitionStateSearch(GtkWidget *, gpointer);
	static void popup_CompStationaryStateSearch(GtkWidget *, gpointer);
	static void popup_CompFormula(GtkWidget *, gpointer);
	
static void popup_CompSetFormalCharge(GtkWidget *, gpointer);	// under construction...
static void popup_CompCreateRS(GtkWidget *, gpointer);		// under construction...
static void popup_CompCycleRS(GtkWidget *, gpointer);		// under construction...
	
	static void popup_TrajView(GtkWidget *, gpointer);
	static void popup_SetOrbital(GtkWidget *, gpointer);
	
	static void popup_HAdd(GtkWidget *, gpointer);
	static void popup_HRemove(GtkWidget *, gpointer);
	
	static void popup_SolvateBox(GtkWidget *, gpointer);
	static void popup_SolvateSphere(GtkWidget *, gpointer);
	
	static void popup_BuilderAmino(GtkWidget *, gpointer);
	static void popup_BuilderNucleic(GtkWidget *, gpointer);
	
	static void popup_Center(GtkWidget *, gpointer);
	static void popup_ClearAll(GtkWidget *, gpointer);
	
	static void popup_EnterCommand(GtkWidget *, gpointer);
};

/*################################################################################################*/

#endif	// GTK_PROJECT_H

// eof
