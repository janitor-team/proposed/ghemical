// CUSTOM_CAMERA.H : an extended ogl_camera class.

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef CUSTOM_CAMERA_H
#define CUSTOM_CAMERA_H

class ghemical_camera;

/*################################################################################################*/

#include <ghemical/typedef.h>
#include <oglappth/ogl_camera.h>

#include "project.h"

/*################################################################################################*/

class custom_camera :
	public ogl_camera
{
	protected:
	
	project * prj;
	
	int ccam_index;
	int wcl_counter;
	
	static int ccam_counter;
	
	friend class oglview_wcl;
	friend class custom_app;
	
	public:
	
	custom_camera(const ogl_object_location &, GLfloat, project *);
	~custom_camera(void);
	
	int GetCCamI(void) { return ccam_index; }
};

/*################################################################################################*/

#endif	// CUSTOM_CAMERA_H

// eof
