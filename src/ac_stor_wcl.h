// AC_STOR_WCL.H : a wcl that stores atom coordinates (base class for plots).

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ghemicalconfig2.h"

#ifndef AC_STOR_WCL_H
#define AC_STOR_WCL_H

#include <ghemical/engine.h>
#include "pangofont_wcl.h"

/*################################################################################################*/

class ac_stor_wcl :
	public pangofont_wcl
{
	private:
	
	vector<atom *> av;
	vector<float *> acv;
	
	public:
	
	ac_stor_wcl(ogl_camera *);
	virtual ~ac_stor_wcl(void);
	
	int StoreAC(engine *);
	int StoreAC(model *, int);
	
	void ShowAC(int);
};

/*################################################################################################*/

#endif	// AC_STOR_WCL_H

// eof
