// GTK_MAIN.CPP

// Copyright (C) 2003 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_main.h"

#include "ghemicalconfig.h"
#include <ghemical/utility.h>

#include "local_i18n.h"

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
//#include <gdk-pixbuf/gdk-pixbuf-loader.h>

/*################################################################################################*/

// define MAIN__ in case mopac7 uses f2c
extern "C" { int MAIN__ (void) {return 0;} }

// the main function.
// ^^^^^^^^^^^^^^^^^^

static gchar * opt_f = NULL;
static gchar * opt_i = NULL;

static GOptionEntry option_entries[] =
{
	{ "file=filename", 'f', 0, G_OPTION_ARG_STRING, & opt_f, _("open a file"), NULL },
	{ "import=filename", 'i', 0, G_OPTION_ARG_STRING, & opt_i, _("import a file"), NULL },
	{ NULL }
};

int main(int argc, char ** argv)
{
	GError * error = NULL;
	
#ifdef ENABLE_NLS
	setlocale(LC_ALL, "");
	bindtextdomain(GETTEXT_PACKAGE, LOCALE_DIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);
#endif	// ENABLE_NLS
	
	GOptionContext * option_context = g_option_context_new(_("- a GTK2 molecular modelling program"));
	g_option_context_add_main_entries(option_context, option_entries, NULL);
	g_option_context_add_group(option_context, gtk_get_option_group(TRUE));
	g_option_context_parse(option_context, & argc, & argv, & error);
	
#ifdef ENABLE_THREADS
	
	//g_thread_init(NULL);					// original...
	if (!g_thread_supported()) g_thread_init(NULL);		// robertix 2008-04-25
	
	gdk_threads_init();
	gdk_threads_enter();
	
#endif	// ENABLE_THREADS
	
	gtk_init(& argc, & argv);
	
	gtk_gl_init(& argc, & argv);
	
	gint major; gint minor;
	gdk_gl_query_version (& major, & minor);
	
	g_print("\n");
	g_print(_("OpenGL extension version - %d.%d\n"), major, minor);
	
#ifndef WIN32
	
	libghemical_init(LIBDATA_PATH);
	strcpy(project::appdata_path, APPDATA_PATH);
	
#else	// WIN32
	
	gchar * modpath = g_win32_get_package_installation_directory_of_module(NULL);
	
	//g_print ("modpath = %s\n", modpath);
	
	// also see: g_win32_locale_filename_from_utf8()
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	// glib-Windows-Compatibility-Functions.html
	
	char tmpstr[256]; char * tmploc;
	
	tmploc = g_stpcpy(tmpstr, modpath);
	tmploc = g_stpcpy(tmploc, "\\share\\libghemical");
	libghemical_init(tmpstr);
	
	tmploc = g_stpcpy(tmpstr, modpath);
	tmploc = g_stpcpy(tmploc, "\\share\\ghemical");
	strcpy(project::appdata_path, tmpstr);
	
	g_free(modpath);
	
#endif	// WIN32
	
	singleton_cleaner<gtk_app> app_cleaner;
	app_cleaner.SetInstance(gtk_app::GetAppX());
	
	if (opt_f != NULL)
	{
		ifstream ifile;
		ifile.open(opt_f, ios::in);
		ReadGPR(* gtk_app::GetPrj(), ifile, false);
		ifile.close();
		
		//char buffer[256];
		//prj->ParseProjectFileNameAndPath(filename);
		//prj->GetProjectFileName(buffer, 256, true);
	}
	else if (opt_i != NULL)
	{
		
#ifdef ENABLE_OPENBABEL
		
		gtk_app::GetPrj()->ImportFile(opt_i, 0);
		
		//gtk_app::GetPrj()->ParseProjectFileNameAndPath(filename);
		
#else	// ENABLE_OPENBABEL
		
		cout << _("Sorry! The file import feature is disabled ; you need to recompile") << endl;
		cout << _("libghemical with --enable-openbabel option in order to fix this.") << endl;
		exit(EXIT_FAILURE);
		
#endif	// ENABLE_OPENBABEL
		
	}
	
	gtk_main();
	
#ifdef ENABLE_THREADS
	
	gdk_threads_leave();
	
#endif	// ENABLE_THREADS
	
	return 0;
}

/*################################################################################################*/

// eof
