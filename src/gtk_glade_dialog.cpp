// GTK_GLADE_DIALOG.CPP

// Copyright (C) 2002 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_glade_dialog.h"

#include "project.h"

#include <sstream>
#include <iostream>
using namespace std;

/*################################################################################################*/

gtk_glade_dialog::gtk_glade_dialog(const char * xmlfile)
{
	ostringstream str;
	str << project::appdata_path << DIR_SEPARATOR << project::appversion << DIR_SEPARATOR << xmlfile << ends;
	
	xml = glade_xml_new(str.str().c_str(), NULL, NULL);
	
	if (xml == NULL)
	{
		ostringstream msg;
		msg << "could not read glade XML file : " << str.str().c_str() << ends;
		assertion_failed(__FILE__, __LINE__, msg.str().c_str());
	}
}

gtk_glade_dialog::~gtk_glade_dialog(void)
{
	// need to do anything here to clean the xml object???
	// need to do anything here to clean the xml object???
	// need to do anything here to clean the xml object???
}

/*################################################################################################*/

// eof
